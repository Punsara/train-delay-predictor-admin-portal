import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CoreModule} from './core/core.module';
import {SharedModule} from './shared/shared.module';
import {LoginComponent} from './views/login/login.component';
import {DefaultLayoutComponent} from './views';
import {FooterComponent} from './views/footer/footer.component';
import {LoginService} from './services/login.service';
import {CommonModule} from '@angular/common';
import {MatDialogModule, MatTableModule} from '@angular/material';
import {NotifierModule} from 'angular-notifier';
import {CookieService} from 'ngx-cookie-service';
import {FileUploadModule} from 'ng2-file-upload';
import {MatFileUploadModule} from 'angular-material-fileupload';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DefaultLayoutComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    CoreModule,
    SharedModule,
    AppRoutingModule,
    MatDialogModule,
    NotifierModule,
    FileUploadModule,
    MatFileUploadModule,
    MatTableModule
  ],
  providers: [LoginService, CookieService],
  exports: [CommonModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
