import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LayoutModule} from '@angular/cdk/layout';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    LayoutModule,
    HttpClientModule,
    FormsModule
  ],
  exports: [
    BrowserAnimationsModule,
    LayoutModule,
    HttpClientModule,
    FormsModule
  ]
})

export class CoreModule {}
