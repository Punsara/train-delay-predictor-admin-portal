import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ResetPassComponent} from './reset-pass.component';


@NgModule({
  declarations: [ResetPassComponent],
  imports: [
    CommonModule
  ],
  exports: [ResetPassComponent]
})
export class ResetPassModule {
}
