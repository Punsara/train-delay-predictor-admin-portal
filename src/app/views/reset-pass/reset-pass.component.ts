import {Component, OnInit} from '@angular/core';
import {NotifierService} from 'angular-notifier';
import {CookieService} from 'ngx-cookie-service';
import {ManageUserService} from '../../services/manage-user.service';
import {Router} from '@angular/router';
import {ManageUsersComponent} from '../manage-users/manage-users.component';

@Component({
  selector: 'app-reset-pass',
  templateUrl: './reset-pass.component.html',
  styleUrls: ['./reset-pass.component.css']
})
export class ResetPassComponent implements OnInit {
  userName: string;
  oldPassword: string;
  newPassword: string;
  seconds = 10;
  requestedUserPassword: string;

  constructor(private router: Router,
              private notifier: NotifierService,
              private cookieService: CookieService,
              private manageUserService: ManageUserService) {
  }

  ngOnInit() {
    const position = this.notifier.getConfig().position;
    position.horizontal.position = 'right';
    position.vertical.position = 'top';
    position.vertical.distance = 80;
  }

  resetPass() {
    const loggedUserName = this.cookieService.get('loggedInUser');
    const loggedRoleType = this.cookieService.get('loggedInUserRoleType');
    const adminDetails = {
      userName: this.userName,
      oldPassword: this.oldPassword,
      newPassword: this.newPassword,
      requestedUserPassword: this.requestedUserPassword,
      requestedUserName: loggedUserName,
      userRole: loggedRoleType,
      lastUpdatedUser: loggedUserName,
    };
    this.manageUserService.resetAdminDetails(adminDetails).subscribe(res => {
      if (res['success'] === true) {
        if (res['redirectToLogin'] === true) {
          this.notifier.show({message: res['msg'] + '\nYou\'ll be Redirect to Login in ' + this.seconds + ' seconds!', type: 'success'});
          const countdown = setInterval(() => {
            this.seconds--;
            if (this.seconds <= 0) {
              clearInterval(countdown);
              ManageUsersComponent.prototype.closeResetPassModal();  // TODO : check is closes Modal before redirect
              this.cookieService.delete('authToken');
              this.router.navigate(['/']).then(() => {
              });
            }
          }, 1000);
        } else {
          this.notifier.show({message: res['msg'], type: 'success'});
        }
      } else {
        this.notifier.show({message: res['msg'], type: 'warning'});
      }
    }, error => {
      console.log(error);
      this.notifier.show({message: error.error['msg'], type: 'error'});
    });
  }
}
