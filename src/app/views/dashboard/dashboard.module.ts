import {NgModule} from '@angular/core';
import {DashboardRoutingModule} from './dashboard-routing.module';
import {DashboardComponent} from './dashboard.component';
import {MatButtonModule, MatCardModule, MatGridListModule, MatIconModule, MatTableModule} from '@angular/material';


@NgModule({
  declarations: [DashboardComponent],
  imports: [
    DashboardRoutingModule,
    MatGridListModule,
    MatTableModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule
  ],
  exports: [DashboardComponent]
})
export class DashboardModule { }
