import {Component, OnInit} from '@angular/core';
import {NotifierService} from 'angular-notifier';
import {Chart} from 'chart.js';
import {DashboardService} from '../../services/dashboard.service';
import {CookieService} from 'ngx-cookie-service';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Saman Perera', weight: 12, symbol: 'Sri Lanka'},
  {position: 2, name: 'Nuwan Perera', weight: 15, symbol: 'Sri Lanka'},
];

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  dataSource = ELEMENT_DATA;
  thisMonthAdminUserCount = 0;
  thisMonthCustomersCount = 0;
  thisMonthSchedules = 0;
  thisMonthDatasets = 0;
  countryWiseDelayProbabilityLabels = [];
  countryWiseDelayProbabilityData = [];

  constructor(private notifier: NotifierService,
              private cookieService: CookieService,
              private dashboardService: DashboardService) {
  }

  ngOnInit() {
    const loggedUserName = this.cookieService.get('loggedInUser');
    const loggedRoleType = this.cookieService.get('loggedInUserRoleType');
    const authDetails = {
      userName: loggedUserName,
      userRole: loggedRoleType
    };
    this.dashboardService.getDashboardData(authDetails).subscribe(res => {
      this.thisMonthAdminUserCount = res['msg'].thisMonthAdminUserCount;
      this.thisMonthCustomersCount = res['msg'].thisMonthCustomersCount;
      this.thisMonthSchedules = res['msg'].thisMonthSchedules;
      this.thisMonthDatasets = res['msg'].thisMonthDatasets;
      this.countryWiseDelayProbabilityLabels = res['msg'].countryWiseDelayProbabilityLabels;
      this.countryWiseDelayProbabilityData = res['msg'].countryWiseDelayProbabilityData;

      this.loadPieChart();
      this.loadLineChart();
    });
  }

  loadLineChart() {
    const lineChart = new Chart('lineChart', {
      type: 'line',
      data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
          label: '# of Votes',
          data: [12, 19, 3, 5, 2, 3],
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {}
      }
    });
  }

  private loadPieChart() {
    const pieChart = new Chart('pieChart', {
      type: 'pie',
      data: {
        labels: this.countryWiseDelayProbabilityLabels,
        datasets: [{
          label: '% of Country Wise Delay',
          data: this.countryWiseDelayProbabilityData,
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {}
      }
    });
  }
}
