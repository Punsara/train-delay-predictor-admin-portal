import {Component, OnInit} from '@angular/core';
import {ManageOtpService} from '../../services/manage-otp.service';

@Component({
  selector: 'app-otp-details',
  templateUrl: './otp-details.component.html',
  styleUrls: ['./otp-details.component.css']
})
export class OtpDetailsComponent implements OnInit {
  displayedColumns: string[] = ['email', 'otp', 'status', 'generatedDateTime', 'lastUpdatedDateTime'];
  dataSource = [];
  loggedUserName;
  loggedRoleType;
  email;

  constructor(private manageOtpService: ManageOtpService) {
  }

  ngOnInit() {
    this.manageOtpService.getOtpByUserEmail({
      userName: this.loggedUserName,
      userRole: this.loggedRoleType,
      email: this.email
    }).subscribe(res => {
      this.dataSource = res['msg'];
    });
  }

  checkStatus(status) {
    switch (status) {
      case 'COMPLETED':
        return '<div class="d-inline-block success text-center pr-3 pl-3">' + 'Completed' + ' </div>';
      case 'PENDING':
        return '<div class="d-inline-block start text-center pr-3 pl-3">' + 'Pending' + ' </div>';
      default:
        return '';
    }
  }
}
