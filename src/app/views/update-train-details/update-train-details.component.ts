import {Component, OnInit} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {ManageTrainsService} from '../../services/manage-trains.service';

@Component({
  selector: 'app-update-train-details',
  templateUrl: './update-train-details.component.html',
  styleUrls: ['./update-train-details.component.css']
})
export class UpdateTrainDetailsComponent implements OnInit {
  trainDetails;
  isUpdateData: boolean;
  country: string;
  trainId: string;
  trainName: string;
  nextStation: string;
  trainCategory: string;
  destination: string;
  latitude: string;
  longitude: string;
  stationFrom: string;
  startedTime: string;
  endTime: string;
  timeDuration: string;

  constructor(private cookieService: CookieService,
              private manageTrainsService: ManageTrainsService) {
  }

  ngOnInit() {
    this.country = this.trainDetails.country;
    this.trainId = this.trainDetails.trainId;
    this.trainName = this.trainDetails.trainName;
    this.nextStation = this.trainDetails.nextStation;
    this.trainCategory = this.trainDetails.trainCategory;
    this.destination = this.trainDetails.destination;
    this.latitude = this.trainDetails.latitude;
    this.longitude = this.trainDetails.longitude;
    this.stationFrom = this.trainDetails.stationFrom;
    this.startedTime = this.trainDetails.startedTime;
    this.endTime = this.trainDetails.endTime;
    this.timeDuration = this.trainDetails.timeDuration;
  }

  saveOrUpdateDetails() {
    const trainData = {
      country: this.country,
      trainId: this.trainId,
      trainName: this.trainName,
      nextStation: this.nextStation,
      trainCategory: this.trainCategory,
      destination: this.destination,
      latitude: this.latitude,
      longitude: this.longitude,
      stationFrom: this.stationFrom,
      startedTime: this.startedTime,
      endTime: this.endTime,
      timeDuration: this.timeDuration
    };
    this.manageTrainsService.saveOrUpdateTrainData(trainData);
  }
}
