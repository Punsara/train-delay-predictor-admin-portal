import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UpdateTrainDetailsComponent} from './update-train-details.component';


@NgModule({
  declarations: [UpdateTrainDetailsComponent],
  imports: [
    CommonModule
  ],
  exports: [UpdateTrainDetailsComponent]
})
export class UpdateTrainDetailsModule {
}
