import {Component, OnInit} from '@angular/core';
import {LoginService} from '../../services/login.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NotifierService} from 'angular-notifier';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  userName: string;
  password: string;
  responseMsg: string;
  error = false;
  warning = false;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private notifier: NotifierService,
              private loginService: LoginService,
              private cookieService: CookieService) {
  }

  ngOnInit() {
    const position = this.notifier.getConfig().position;
    position.horizontal.position = 'right';
    position.vertical.position = 'top';
    position.vertical.distance = 80;
  }

  authenticateAdmin() {
    const adminCredentials = {userName: this.userName, password: this.password};
    this.loginService.authenticateUser(adminCredentials).subscribe(res => {
      if (res['success'] === true) {
        this.router.navigate(['/home'], {relativeTo: this.route}).then(() => {
          this.cookieService.set('authToken', res['token']);
          this.cookieService.set('loggedInUserRoleType', res['roleType']);
          this.cookieService.set('loggedInUser', this.userName);
          this.notifier.show({message: 'Welcome to TDP-Admin!', type: 'success'});
        });
      } else {
        // this.notifier.show({message: res['msg'], type: 'warning'});
        this.responseMsg = res['msg'];
        this.error = true;
      }
    }, (error) => {
      console.log(error);
      // this.notifier.show({message: error.error['msg'], type: 'error'});
      this.responseMsg = error.error['msg'];
      this.error = true;
    });
  }
}
