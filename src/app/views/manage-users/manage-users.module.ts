import {NgModule} from '@angular/core';
import {ManageUsersRoutingModule} from './manage-users-routing.module';
import {ManageUsersComponent} from './manage-users.component';
import {MatButtonModule, MatCardModule, MatDialogModule, MatFormFieldModule, MatInputModule, MatNativeDateModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import {CommonModule} from '@angular/common';
import {RegisterUserComponent} from '../register-user/register-user.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {ResetPassComponent} from '../reset-pass/reset-pass.component';

@NgModule({
  declarations: [ManageUsersComponent, RegisterUserComponent, ResetPassComponent],
  imports: [
    ManageUsersRoutingModule,
    MatCardModule,
    MatFormFieldModule,
    FormsModule,
    MatSelectModule,
    CommonModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatDialogModule,
    MatIconModule,
    MatGridListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatGridListModule,
    MatTableModule,
    MatIconModule
  ],
  entryComponents: [
    RegisterUserComponent,
    ResetPassComponent
  ],
})
export class ManageUsersModule { }
