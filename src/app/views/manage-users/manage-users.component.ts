import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {RegisterUserComponent} from '../register-user/register-user.component';
import {ManageUserService} from '../../services/manage-user.service';
import {Utility} from '../../util/Utility';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';
import {ResetPassComponent} from '../reset-pass/reset-pass.component';

export interface UserRole {
  value: string;
  text: string;
}

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.css']
})
export class ManageUsersComponent implements OnInit {
  displayedColumns: string[] = ['name', 'userName', 'email', 'roleType', 'createdDateTime', 'lastUpdatedUser',
    'lastUpdatedDateTime', 'resetPass'];
  dataSource;
  adminRegisterPopup;
  resetPassPopUp;
  roleTypes: UserRole[];
  selectedRoleType: string;
  selectedDateFrom: string;
  selectedDateTo: string;
  isAuthorizeToAddUsers: boolean;
  isAuthorizeToResetPass: boolean;

  constructor(private router: Router,
              private dialogPopUp: MatDialog,
              private cookieService: CookieService,
              private manageUserService: ManageUserService) {
  }

  openDialog() {
    this.adminRegisterPopup = this.dialogPopUp.open(RegisterUserComponent);
    this.adminRegisterPopup.afterClosed().subscribe(res => {
      console.log('Closed');
      this.loadTableData();
    });
  }

  ngOnInit() {
    this.loadFilters();
  }

  loadFilters() {
    const loggedUserName = this.cookieService.get('loggedInUser');
    const loggedRoleType = this.cookieService.get('loggedInUserRoleType');
    const filtration = {
      userName: loggedUserName,
      userRole: loggedRoleType
    };
    this.isAuthorizeToAddUsers = Utility.userRoleSuperAdmin === loggedRoleType;
    this.isAuthorizeToResetPass = Utility.userRoleSuperAdmin === loggedRoleType || Utility.userRoleAdmin === loggedRoleType;
    this.manageUserService.getManageUserFilters(filtration).subscribe(res => {
      this.roleTypes = res['msg'].roleTypes;
      this.selectedDateFrom = res['msg'].dateFrom;
      this.selectedDateTo = res['msg'].dateTo;
      this.loadTableData();
    });
  }

  loadTableData() {
    const loggedUserName = this.cookieService.get('loggedInUser');
    const loggedRoleType = this.cookieService.get('loggedInUserRoleType');
    const selectedRoleType = undefined === this.selectedRoleType ? Utility.filtrationALL : this.selectedRoleType;
    this.manageUserService.viewFilteredUsers({
      userName: loggedUserName,
      userRole: loggedRoleType,
      roleType: selectedRoleType,
      dateFrom: this.selectedDateFrom,
      dateTo: this.selectedDateTo
    }).subscribe(res => {
      this.dataSource = res['msg'];
    });
  }

  resetPass() {
    this.resetPassPopUp = this.dialogPopUp.open(ResetPassComponent);
    this.resetPassPopUp.afterClosed().subscribe(res => {
      console.log('Closed');
    });
  }

  closeResetPassModal() {
    this.resetPassPopUp.close();
  }
}
