import {Component, OnInit} from '@angular/core';
import {UserRole} from '../manage-users/manage-users.component';
import {MatDialog} from '@angular/material/dialog';
import {CookieService} from 'ngx-cookie-service';
import {Utility} from '../../util/Utility';
import {ManageTrainsService} from '../../services/manage-trains.service';
import {UpdateTrainDetailsComponent} from '../update-train-details/update-train-details.component';

@Component({
  selector: 'app-manage-trains',
  templateUrl: './manage-trains.component.html',
  styleUrls: ['./manage-trains.component.css']
})
export class ManageTrainsComponent implements OnInit {
  displayedColumns: string[] = ['country', 'trainId', 'trainCategory', 'status', 'delayMinutes', 'stationFrom', 'nextStation',
    'destination', 'latitude', 'longitude', 'startedTime', 'endTime', 'timeDuration', 'editDetails'];
  dataSource;
  roleTypes: UserRole[];
  countries: [];
  selectedCountry: string;
  selectedDateFrom: string;
  selectedDateTo: string;
  isAuthorizeToAddUsers: boolean;
  editTrainDetailsPopUp;

  constructor(private dialogPopUp: MatDialog,
              private cookieService: CookieService,
              private manageTrainsService: ManageTrainsService) {
  }

  ngOnInit() {
    this.loadFilters();
  }

  loadFilters() {
    const loggedUserName = this.cookieService.get('loggedInUser');
    const loggedRoleType = this.cookieService.get('loggedInUserRoleType');
    const filtration = {
      userName: loggedUserName,
      userRole: loggedRoleType
    };
    this.isAuthorizeToAddUsers = Utility.userRoleSuperAdmin === loggedRoleType;
    this.manageTrainsService.getManageTrainFilters(filtration).subscribe(res => {
      this.countries = res['msg'].countries;
      this.selectedDateFrom = res['msg'].dateFrom;
      this.selectedDateTo = res['msg'].dateTo;
      this.loadTableData();
    });
  }

  loadTableData() {
    const loggedUserName = this.cookieService.get('loggedInUser');
    const loggedRoleType = this.cookieService.get('loggedInUserRoleType');
    const selectedCountry = undefined === this.selectedCountry ? Utility.filtrationALL : this.selectedCountry;
    const filtration = {
      userName: loggedUserName,
      userRole: loggedRoleType,
      country: selectedCountry,
      dateFrom: this.selectedDateFrom,
      dateTo: this.selectedDateTo
    };
    this.manageTrainsService.viewFilteredManageTrainData(filtration).subscribe(res => {
      console.log(res['msg']);
      this.dataSource = res['msg'];
    });
  }

  saveTrainDetails() {
    UpdateTrainDetailsComponent.prototype.isUpdateData = false;
    UpdateTrainDetailsComponent.prototype.trainDetails = {};
    this.editTrainDetailsPopUp = this.dialogPopUp.open(UpdateTrainDetailsComponent);
    this.editTrainDetailsPopUp.afterClosed().subscribe(res => {
      console.log('Closed');
    });
  }

  editTrainDetails(trainDetails: any) {
    console.log(trainDetails);
    UpdateTrainDetailsComponent.prototype.trainDetails = trainDetails;
    UpdateTrainDetailsComponent.prototype.isUpdateData = true;
    this.editTrainDetailsPopUp = this.dialogPopUp.open(UpdateTrainDetailsComponent);
    this.editTrainDetailsPopUp.afterClosed().subscribe(res => {
      console.log('Closed');
    });
  }
}
