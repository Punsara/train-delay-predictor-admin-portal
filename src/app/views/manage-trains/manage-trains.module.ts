import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ManageTrainsComponent} from './manage-trains.component';
import {ManageTrainsRoutingModule} from './manage-trains-routing.module';
import {
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatNativeDateModule,
  MatSelectModule,
  MatTableModule
} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {UpdateTrainDetailsComponent} from '../update-train-details/update-train-details.component';

@NgModule({
  declarations: [ManageTrainsComponent, UpdateTrainDetailsComponent],
  imports: [
    ManageTrainsRoutingModule,
    CommonModule,
    MatCardModule,
    MatGridListModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDatepickerModule,
    FormsModule,
    MatIconModule,
    MatTableModule,
    MatButtonModule,
    MatInputModule,
    MatNativeDateModule,
  ],
  entryComponents: [
    UpdateTrainDetailsComponent
  ]
})
export class ManageTrainsModule {
}
