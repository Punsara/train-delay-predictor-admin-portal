import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ManageTrainsComponent} from './manage-trains.component';


const routes: Routes = [
  {
    path: '',
    component: ManageTrainsComponent,
    data: {
      title: 'Manage Trains'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageTrainsRoutingModule {
}
