import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UploadDataComponent} from './upload-data.component';

const routes: Routes = [
  {
    path: '',
    component: UploadDataComponent,
    data: {
      title: 'Upload Data'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UploadDataRoutingModule { }
