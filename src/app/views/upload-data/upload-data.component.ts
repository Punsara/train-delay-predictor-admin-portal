import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {CsvUploaderComponent} from '../csv-uploader/csv-uploader.component';
import {UploadDataService} from '../../services/upload-data.service';
import {Utility} from '../../util/Utility';
import {UploadDataDetailsComponent} from '../upload-data-details/upload-data-details.component';
import {CookieService} from 'ngx-cookie-service';

interface Country {
  _id: bigint;
  countryName: string;
  alpha3Code: string;
  region: string;
  subRegion: string;
}

@Component({
  selector: 'app-upload-data',
  templateUrl: './upload-data.component.html',
  styleUrls: ['./upload-data.component.css']
})
export class UploadDataComponent implements OnInit {
  displayedColumns: string[] = ['country', 'status', 'createdDateTime', 'lastUpdatedUser', 'lastUpdatedDateTime', 'dataSetDetails'];
  uploadCsvPopUp;
  viewDataDetailsPopUp;
  lastUpdatedDateTime;
  selectedCountry;
  selectedDateFrom;
  selectedDateTo;
  dataSource;
  countries: Country[];

  constructor(private dialogPopUp: MatDialog,
              private cookieService: CookieService,
              private uploadDataService: UploadDataService) {
  }

  ngOnInit() {
    this.loadFilters();
  }

  loadFilters() {
    const loggedUserName = this.cookieService.get('loggedInUser');
    const loggedRoleType = this.cookieService.get('loggedInUserRoleType');
    const filtration = {
      userName: loggedUserName,
      userRole: loggedRoleType
    };
    this.uploadDataService.getUploadDataFilters(filtration).subscribe(res => {
      this.countries = res['msg'].countries;
      this.selectedDateFrom = res['msg'].dateFrom;
      this.selectedDateTo = res['msg'].dateTo;
      this.lastUpdatedDateTime = res['msg'].lastUpdatedDateTime;
      this.loadTableData();
    });
  }

  loadTableData() {
    const loggedUserName = this.cookieService.get('loggedInUser');
    const loggedRoleType = this.cookieService.get('loggedInUserRoleType');
    const selectedCountry = undefined === this.selectedCountry ? Utility.filtrationALL : this.selectedCountry;
    const filtration = {
      userName: loggedUserName,
      userRole: loggedRoleType,
      country: selectedCountry,
      dateFrom: this.selectedDateFrom,
      dateTo: this.selectedDateTo
    };
    this.uploadDataService.viewFilteredUploadedData(filtration).subscribe(res => {
      this.dataSource = res['msg'];
    });
  }

  viewDetails(uploadData: any) {
    UploadDataDetailsComponent.prototype.dataSource = uploadData;
    this.viewDataDetailsPopUp = this.dialogPopUp.open(UploadDataDetailsComponent);
    this.viewDataDetailsPopUp.afterClosed().subscribe(res => {
      console.log('Closed');
    });
  }

  openDialog() {
    this.uploadCsvPopUp = this.dialogPopUp.open(CsvUploaderComponent);
    this.uploadCsvPopUp.afterClosed().subscribe(res => {
      console.log('Closed');
      this.loadTableData();
    });
  }
}
