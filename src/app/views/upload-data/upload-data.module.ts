import {NgModule} from '@angular/core';
import {UploadDataComponent} from './upload-data.component';
import {UploadDataRoutingModule} from './upload-data-routing.module';

// @ts-ignore
import {
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatNativeDateModule,
  MatOptionModule,
  MatSelectModule,
  MatTableModule,
} from '@angular/material';
import {CommonModule} from '@angular/common';
import {CsvUploaderComponent} from '../csv-uploader/csv-uploader.component';
import {FormsModule} from '@angular/forms';
import {FileUploadModule} from 'ng2-file-upload';
import {MatFileUploadModule} from 'angular-material-fileupload';
import {UploadDataDetailsComponent} from '../upload-data-details/upload-data-details.component';


@NgModule({
  declarations: [UploadDataComponent, CsvUploaderComponent, UploadDataDetailsComponent],
  imports: [
    UploadDataRoutingModule,
    MatCardModule,
    MatFormFieldModule,
    MatTableModule,
    MatOptionModule,
    CommonModule,
    MatSelectModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    MatGridListModule,
    MatIconModule,
    MatButtonModule,
    FormsModule,
    FileUploadModule,
    MatFileUploadModule
  ],
  exports: [UploadDataComponent],
  entryComponents: [
    CsvUploaderComponent,
    UploadDataDetailsComponent
  ]
})
export class UploadDataModule {
}
