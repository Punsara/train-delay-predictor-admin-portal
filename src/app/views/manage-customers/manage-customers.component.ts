import {Component, OnInit} from '@angular/core';
import {ManageCustomerService} from '../../services/manage-customer.service';
import {Utility} from '../../util/Utility';
import {MatDialog} from '@angular/material/dialog';
import {OtpDetailsComponent} from '../otp-details/otp-details.component';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-manage-customers',
  templateUrl: './manage-customers.component.html',
  styleUrls: ['./manage-customers.component.css']
})
export class ManageCustomersComponent implements OnInit {
  displayedColumns: string[] = ['firstName', 'lastName', 'email', 'country', 'createdDateTime', 'lastUpdatedDateTime', 'viewOtp'];
  dataSource = [];
  selectedCustomer;
  selectedDateFrom;
  selectedDateTo;
  otpDetailsPopUp;

  constructor(private dialogPopUp: MatDialog,
              private cookieService: CookieService,
              private manageCustomerService: ManageCustomerService) {
  }

  ngOnInit() {
    this.loadFilters();
  }

  loadFilters() {
    const loggedUserName = this.cookieService.get('loggedInUser');
    const loggedRoleType = this.cookieService.get('loggedInUserRoleType');
    const filtration = {
      userName: loggedUserName,
      userRole: loggedRoleType
    };
    this.manageCustomerService.getManageCustomerFilters(filtration).subscribe(res => {
      this.selectedDateFrom = res['msg'].dateFrom;
      this.selectedDateTo = res['msg'].dateTo;
      this.loadTableData();
    });
  }

  loadTableData() {
    const loggedUserName = this.cookieService.get('loggedInUser');
    const loggedRoleType = this.cookieService.get('loggedInUserRoleType');
    const selectedCustomerName = undefined === this.selectedCustomer ? Utility.filtrationALL : this.selectedCustomer;
    this.manageCustomerService.viewFilteredCustomers({
      userName: loggedUserName,
      userRole: loggedRoleType,
      name: selectedCustomerName,
      dateFrom: this.selectedDateFrom,
      dateTo: this.selectedDateTo
    }).subscribe(res => {
      this.dataSource = res['msg'];
    });
  }

  async viewOtpDetails(email: any) {
    const loggedUserName = this.cookieService.get('loggedInUser');
    const loggedRoleType = this.cookieService.get('loggedInUserRoleType');
    OtpDetailsComponent.prototype.email = email;
    OtpDetailsComponent.prototype.loggedUserName = loggedUserName;
    OtpDetailsComponent.prototype.loggedRoleType = loggedRoleType;
    this.otpDetailsPopUp = this.dialogPopUp.open(OtpDetailsComponent);
    this.otpDetailsPopUp.afterClosed().subscribe(res => {
      console.log('Closed');
    });
  }
}
