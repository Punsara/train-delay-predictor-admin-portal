import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ManageCustomersComponent} from './manage-customers.component';
import {
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatNativeDateModule,
  MatSelectModule,
  MatTableModule,
} from '@angular/material';
import {ManageCustomersRoutingModule} from './manage-customers-routing.module';
import {FormsModule} from '@angular/forms';
import {OtpDetailsComponent} from '../otp-details/otp-details.component';


@NgModule({
  declarations: [ManageCustomersComponent, OtpDetailsComponent],
  imports: [
    CommonModule,
    MatTableModule,
    ManageCustomersRoutingModule,
    MatCardModule,
    MatGridListModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    FormsModule,
    MatIconModule,
    MatButtonModule
  ],
  entryComponents: [
    OtpDetailsComponent
  ],
})
export class ManageCustomersModule {
}
