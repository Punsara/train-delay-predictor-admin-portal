import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ManageCustomersComponent} from './manage-customers.component';


const routes: Routes = [
  {
    path: '',
    component: ManageCustomersComponent,
    data: {
      title: 'Manage Customers'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ManageCustomersRoutingModule {
}
