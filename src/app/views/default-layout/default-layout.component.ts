import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {map, shareReplay} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
import {Utility} from '../../util/Utility';

@Component({
  selector: 'app-default-layout',
  templateUrl: './default-layout.component.html',
  styleUrls: ['./default-layout.component.css']
})
export class DefaultLayoutComponent implements OnInit {
  loggedInUser: string;
  currentTime: string = new Date().toString().split('GMT')[0];
  isAuthorizeToManageUsers: boolean;
  isAuthorizeToManageCustomers: boolean;
  isAuthorizeToManageTrains: boolean;
  isAuthorizeToUploadData: boolean;

  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches), shareReplay());

  constructor(private route: ActivatedRoute,
              private router: Router,
              private breakpointObserver: BreakpointObserver,
              private cookieService: CookieService) {
    setInterval(() => {
      this.currentTime = new Date().toString().split('GMT')[0];
    }, 1);
  }

  ngOnInit() {
    this.manageInit();
  }

  manageInit() {
    const authToken = this.cookieService.get('authToken');
    if (!authToken) {
      this.router.navigate(['/']).then(() => {
      });
      return;
    }
    const loggedUserRole = this.cookieService.get('loggedInUserRoleType');
    this.manageAuthorization(loggedUserRole);
    this.loggedInUser = this.cookieService.get('loggedInUser');
  }

  logOut() {
    this.cookieService.delete('authToken');
    this.router.navigate(['/']).then(() => {
    });
  }

  manageAuthorization(userRole) {
    switch (userRole) {
      case Utility.userRoleSuperAdmin:
        this.isAuthorizeToManageUsers = true;
        this.isAuthorizeToManageCustomers = true;
        this.isAuthorizeToManageTrains = true;
        this.isAuthorizeToUploadData = true;
        break;
      case Utility.userRoleAdmin:
        this.isAuthorizeToManageUsers = true;
        this.isAuthorizeToManageCustomers = true;
        this.isAuthorizeToManageTrains = true;
        this.isAuthorizeToUploadData = true;
        break;
      case Utility.userRoleCallCenter:
        this.isAuthorizeToManageUsers = false;
        this.isAuthorizeToManageCustomers = true;
        this.isAuthorizeToManageTrains = false;
        this.isAuthorizeToUploadData = false;
        break;
      default:
        break;
    }
  }
}
