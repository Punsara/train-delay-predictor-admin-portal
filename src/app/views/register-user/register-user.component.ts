import {Component, OnInit} from '@angular/core';
import {ManageUserService} from '../../services/manage-user.service';
import {NotifierService} from 'angular-notifier';
import {CookieService} from 'ngx-cookie-service';

export interface UserRole {
  value: string;
  text: string;
}

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent implements OnInit {
  name: string;
  email: string;
  roleTypes: UserRole[];
  selectedRoleType: string;
  userName: string;
  password: string;

  constructor(private notifier: NotifierService,
              private cookieService: CookieService,
              private manageUserService: ManageUserService) {
  }

  register() {
    const loggedUserName = this.cookieService.get('loggedInUser');
    const loggedRoleType = this.cookieService.get('loggedInUserRoleType');
    const adminDetails = {
      name: this.name,
      email: this.email,
      roleType: this.selectedRoleType,
      userName: this.userName,
      password: this.password,
      userRole: loggedRoleType,
      lastUpdatedUser: loggedUserName,
    };
    this.manageUserService.registerAdmin(adminDetails).subscribe(res => {
      console.log(res);
      if (res['success'] === true) {
        this.notifier.show({message: res['msg'], type: 'success'});
      } else {
        this.notifier.show({message: res['msg'], type: 'warning'});
      }
    }, error => {
      console.log(error);
      this.notifier.show({message: error.error['msg'], type: 'error'});
    });
  }

  ngOnInit() {
    const position = this.notifier.getConfig().position;
    position.horizontal.position = 'right';
    position.vertical.position = 'top';
    position.vertical.distance = 80;
    this.loadFilters();
  }

  loadFilters() {
    const loggedUserName = this.cookieService.get('loggedInUser');
    const loggedRoleType = this.cookieService.get('loggedInUserRoleType');
    const filtration = {
      userName: loggedUserName,
      userRole: loggedRoleType
    };
    this.manageUserService.getManageUserFilters(filtration).subscribe(res => {
      this.roleTypes = res['msg'].roleTypes;
    });
  }
}
