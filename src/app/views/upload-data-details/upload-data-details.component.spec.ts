import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UploadDataDetailsComponent} from './upload-data-details.component';

describe('UploadDataDetailsComponent', () => {
  let component: UploadDataDetailsComponent;
  let fixture: ComponentFixture<UploadDataDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UploadDataDetailsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadDataDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
