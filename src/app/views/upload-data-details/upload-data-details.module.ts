import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UploadDataDetailsComponent} from './upload-data-details.component';
import {MatButtonModule, MatCardModule, MatIconModule, MatTableModule} from '@angular/material';

@NgModule({
  declarations: [UploadDataDetailsComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule
  ],
  exports: [UploadDataDetailsComponent]
})
export class UploadDataDetailsModule {
}
