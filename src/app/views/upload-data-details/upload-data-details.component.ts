import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-upload-data-details',
  templateUrl: './upload-data-details.component.html',
  styleUrls: ['./upload-data-details.component.css']
})
export class UploadDataDetailsComponent implements OnInit {
  displayedColumns: string[] = ['trainId', 'trainCategory', 'status', 'delayMinutes', 'stationFrom', 'nextStation',
    'destination', 'latitude', 'longitude', 'startedTime', 'endTime', 'timeDuration'];
  dataSource;

  constructor() {
  }

  ngOnInit() {
  }

}
