import {Component, OnInit} from '@angular/core';
import {UploadDataService} from '../../services/upload-data.service';
import {NotifierService} from 'angular-notifier';
import {CookieService} from 'ngx-cookie-service';
import {UploadDataComponent} from '../upload-data/upload-data.component';

interface Country {
  _id: bigint;
  countryName: string;
  alpha3Code: string;
  region: string;
  subRegion: string;
}

@Component({
  selector: 'app-csv-uploader',
  templateUrl: './csv-uploader.component.html',
  styleUrls: ['./csv-uploader.component.css']
})
export class CsvUploaderComponent implements OnInit {
  countries: Country[];
  selectedCountry: string;
  csv: File;

  constructor(private notifier: NotifierService,
              private cookieService: CookieService,
              private uploadDataService: UploadDataService) {
    const loggedUserName = this.cookieService.get('loggedInUser');
    const loggedRoleType = this.cookieService.get('loggedInUserRoleType');
    const filtration = {
      userName: loggedUserName,
      userRole: loggedRoleType
    };
    this.uploadDataService.getUploadDataFilters(filtration).subscribe(res => {
      this.countries = res['msg'].countries;
    });
  }

  ngOnInit() {
    const position = this.notifier.getConfig().position;
    position.horizontal.position = 'right';
    position.vertical.position = 'top';
    position.vertical.distance = 80;
  }

  selectFile(event) {
    if (event.target.files.length > 0) {
      this.csv = event.target.files[0];
    }
  }

  upload() {
    const loggedUserName = this.cookieService.get('loggedInUser');
    const loggedRoleType = this.cookieService.get('loggedInUserRoleType');

    const fileUploadData = new FormData();
    fileUploadData.append('userName', loggedUserName);
    fileUploadData.append('userRole', loggedRoleType);
    fileUploadData.append('country', this.selectedCountry);
    fileUploadData.append('lastUpdatedUser', loggedRoleType);
    fileUploadData.append('file', this.csv);

    this.uploadDataService.uploadDataSet(fileUploadData).subscribe(res => {
      if (res['success'] === true) {
        UploadDataComponent.prototype.lastUpdatedDateTime = res['msg'].lastUpdatedDateTime;
        this.notifier.show({message: res['msg'], type: 'success'});
      } else {
        this.notifier.show({message: res['msg'], type: 'error'});
      }
    }, (error) => {
      console.log(error);
      this.notifier.show({message: error.error['msg'], type: 'error'});
    });
  }
}
