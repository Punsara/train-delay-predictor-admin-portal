import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CsvUploaderComponent} from './csv-uploader.component';
import {MatButtonModule, MatCardModule, MatFormFieldModule, MatIconModule, MatOptionModule, MatSelectModule} from '@angular/material';
import {FileUploadModule} from 'ng2-file-upload';
import {MatFileUploadComponent, MatFileUploadModule} from 'angular-material-fileupload';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [CsvUploaderComponent, MatFileUploadComponent],
  imports: [
    CommonModule,
    FileUploadModule,
    MatIconModule,
    MatCardModule,
    MatFormFieldModule,
    MatOptionModule,
    MatSelectModule,
    MatButtonModule,
    FileUploadModule,
    MatFileUploadModule,
    FormsModule
  ],
  exports: [CsvUploaderComponent]
})
export class CsvUploaderModule {
}
