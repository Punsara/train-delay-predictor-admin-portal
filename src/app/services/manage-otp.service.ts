import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Utility} from '../util/Utility';

@Injectable({
  providedIn: 'root'
})
export class ManageOtpService {

  constructor(private http: HttpClient) {
  }

  getOtpByUserEmail(email: { email: any, userName: any, userRole: any }) {
    return this.http.post(Utility.adminBackendAPIURL + '/otpDetails', email);
  }
}
