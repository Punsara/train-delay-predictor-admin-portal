import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Utility} from '../util/Utility';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) {
  }

  authenticateUser(credentials: any) {
    return this.http.post(Utility.adminBackendAPIURL + '/login', credentials);
  }
}
