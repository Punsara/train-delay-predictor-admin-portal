import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Utility} from '../util/Utility';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) {
  }

  getDashboardData(authDetails) {
    return this.http.post(Utility.adminBackendAPIURL + '/dashboard', authDetails);
  }
}
