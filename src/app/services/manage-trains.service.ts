import {Injectable} from '@angular/core';
import {Utility} from '../util/Utility';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ManageTrainsService {

  constructor(private http: HttpClient) {
  }

  getManageTrainFilters(filtration: any) {
    return this.http.post(Utility.adminBackendAPIURL + '/manageTrain', filtration);
  }

  viewFilteredManageTrainData(filtration: any) {
    return this.http.post(Utility.adminBackendAPIURL + '/viewTrainData', filtration);
  }

  saveOrUpdateTrainData(trainData: any) {
    return this.http.post(Utility.adminBackendAPIURL + '/updateTrainData', trainData);
  }
}
