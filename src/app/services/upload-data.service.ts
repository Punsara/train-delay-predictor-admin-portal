import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Utility} from '../util/Utility';

@Injectable({
  providedIn: 'root'
})
export class UploadDataService {

  constructor(private http: HttpClient) {
  }

  getUploadDataFilters(filtration: any) {
    return this.http.post(Utility.adminBackendAPIURL + '/manageUploadData', filtration);
  }

  uploadDataSet(fileUploadData: any) {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    return this.http.post(Utility.adminBackendAPIURL + '/uploadData', fileUploadData, {headers});


    // return new Promise((resolve, reject) => {
    //   const xhr = new XMLHttpRequest();
    //   xhr.onreadystatechange = () => {
    //     if (xhr.readyState === 4) {
    //       if (xhr.status === 200) {
    //         resolve(JSON.parse(xhr.response));
    //       } else {
    //         reject(xhr.response);
    //       }
    //     }
    //   };
    //   console.log('fileUploadData in service');
    //   console.log(fileUploadData);
    //   xhr.open('POST', Utility.adminBackendAPIURL + '/uploadData', true);
    //   xhr.send(fileUploadData);
    // });
  }

  viewFilteredUploadedData(filtration: any) {
    return this.http.post(Utility.adminBackendAPIURL + '/viewUploadData', filtration);
    //  , {
    //       observe: 'body', params: undefined, reportProgress: false, responseType: 'arraybuffer', withCredentials: false,
    //       headers: {'Access-Control-Allow-Origin': '*'}}
    //  {'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'}
  }
}
