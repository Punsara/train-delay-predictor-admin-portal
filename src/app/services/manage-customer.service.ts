import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Utility} from '../util/Utility';

@Injectable({
  providedIn: 'root'
})
export class ManageCustomerService {

  constructor(private http: HttpClient) {
  }

  viewFilteredCustomers(filtration: any) {
    return this.http.post(Utility.adminBackendAPIURL + '/viewCustomers', filtration);
  }

  getManageCustomerFilters(filtration: any) {
    return this.http.post(Utility.adminBackendAPIURL + '/manageCustomer', filtration);
  }
}
