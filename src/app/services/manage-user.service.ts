import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Utility} from '../util/Utility';

@Injectable({
  providedIn: 'root'
})
export class ManageUserService {

  constructor(private http: HttpClient) {
  }

  registerAdmin(adminDetails: any) {
    return this.http.post(Utility.adminBackendAPIURL + '/register', adminDetails);
  }

  viewFilteredUsers(filtration: any) {
    return this.http.post(Utility.adminBackendAPIURL + '/viewUsers', filtration);
  }

  getManageUserFilters(filtration: any) {
    return this.http.post(Utility.adminBackendAPIURL + '/manageUsers', filtration);
  }

  resetAdminDetails(adminDetails: any) {
    return this.http.post(Utility.adminBackendAPIURL + '/resetPass', adminDetails);
  }
}
