import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './views/login/login.component';
import {DefaultLayoutComponent} from './views';


const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
  },
  {
    path: 'home',
    redirectTo: 'home/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: DefaultLayoutComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'manage-users',
        loadChildren: () => import('./views/manage-users/manage-users.module').then(m => m.ManageUsersModule)
      },
      {
        path: 'manage-customers',
        loadChildren: () => import('./views/manage-customers/manage-customers.module').then(m => m.ManageCustomersModule)
      },
      {
        path: 'manage-trains',
        loadChildren: () => import('./views/manage-trains/manage-trains.module').then(m => m.ManageTrainsModule)
      },
      {
        path: 'upload-data',
        loadChildren: () => import('./views/upload-data/upload-data.module').then(m => m.UploadDataModule)
      }
    ]
  }
  // {
  //   path: '404',
  //   component: null,
  //   data: {
  //     title: 'Page 404'
  //   }
  // },
  // {
  //   path: '500',
  //   component: null,
  //   data: {
  //     title: 'Page 500'
  //   }
  // },
  // {path: '**', component: null}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
