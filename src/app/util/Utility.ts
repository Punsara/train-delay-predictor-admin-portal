export class Utility {
  public static adminBackendAPIURL = 'https://train-delay-predictor-admin.herokuapp.com/api';
  public static filtrationALL = 'ALL';
  public static userRoleSuperAdmin = 'SUPER_ADMIN';
  public static userRoleAdmin = 'ADMIN';
  public static userRoleCallCenter = 'CALL_CENTER';
}
